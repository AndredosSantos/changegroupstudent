import axios from "axios";
import { useState, useEffect } from "react";
const App = () => {
  const [groupId, setGroupId] = useState("");
  const [email, setEmail] = useState("");
  const [token, setToken] = useState("");
  const [students, setStudents] = useState([]);

  const onClickFunction = () => {
    const loginData = {
      email: email,
      password: email,
    };

    axios
      .post("https://api.kenzie.com.br/v1/teaching/users/sign_in/", loginData)
      .then(({ data }) => {
        setToken(data.data.token);
      });
  };
  useEffect(() => {
    if (token !== "") {
      axios
        .get("https://api.kenzie.com.br/v1/teaching/students", {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then(({ data }) => {
          setStudents(
            data.data.filter(
              (currentStudent) => currentStudent.students_group_id === groupId
            )
          );
        });
    }
    //eslint-disable-next-line
  }, [token]);

  return (
    <div>
      <input type="text" onChange={(e) => setGroupId(e.target.value)} />
      <input type="text" onChange={(e) => setEmail(e.target.value)} />
      <button type="button" onClick={onClickFunction}>
        Send
      </button>

      <ul>
        {students.map((currentStudent) => (
          <li>
            <p>{currentStudent.name}</p>
            <p>{currentStudent.id}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default App;
